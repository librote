# Makefile for librote
#
# Copyright (c) 2004 Bruno T. C. de Oliveira
# 
# LICENSE INFORMATION:
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
# Copyright (c) 2002 Bruno T. C. de Oliveira

OBJECTS = inject.o inject_csi.o rote.o rote_keymap.o

ifeq (@ROTE_LIB_NAME@,rotew)
    OBJECTS += utf8.o
endif

ROTE_VERSION=@PACKAGE_VERSION@

ROTE_SONAME=lib@ROTE_LIB_NAME@.so.0
ROTE_LIB_NAME=lib@ROTE_LIB_NAME@.so

CC=@CC@
CFLAGS=@CFLAGS@ -Wall -fPIC
LIBS=@LIBS@
LDFLAGS=@LDFLAGS@
prefix=@prefix@
exec_prefix=@exec_prefix@
libdir=@libdir@
includedir=@includedir@
bindir=@bindir@

all: $(ROTE_LIB_NAME).$(ROTE_VERSION)

install: all
	mkdir -p $(DESTDIR)$(includedir)/rote
	rm -f $(DESTDIR)$(includedir)/rote/@ROTE_LIB_NAME@.h
	cp rote.h $(DESTDIR)$(includedir)/rote/@ROTE_LIB_NAME@.h
	mkdir -p $(DESTDIR)$(libdir)
	cp $(ROTE_LIB_NAME).$(ROTE_VERSION) $(DESTDIR)$(libdir)
	cd $(DESTDIR)$(libdir) && ln -sf $(ROTE_LIB_NAME).$(ROTE_VERSION) $(ROTE_LIB_NAME)
	cd $(DESTDIR)$(libdir) && ln -sf $(ROTE_LIB_NAME).$(ROTE_VERSION) $(ROTE_SONAME)
	chmod 755 rote-config
	mkdir -p $(DESTDIR)$(bindir)
	cp -p rote-config $(DESTDIR)$(bindir)
	@echo "-----------------------------------------------------------"
	@echo "ROTE - Our Own Terminal Emulation Library v$(ROTE_VERSION)"
	@echo
	@echo "Include files installed at: $(DESTDIR)$(includedir)"
	@echo "Library files installed at: $(DESTDIR)$(libdir)"
	@echo "rote-config executable    : $(DESTDIR)$(bindir)/rote-config"
	@echo
	@echo "To find out what compiler arguments you should use to"
	@echo "compile programs that use rote, use the rote-config"
	@echo "program (make sure $(DESTDIR)$(bindir) is in your path)."
	@echo "-----------------------------------------------------------"

.c.o:
	$(CC) -c $(CFLAGS) $<

$(ROTE_LIB_NAME).$(ROTE_VERSION): $(OBJECTS)
	$(CC) $(CFLAGS) -shared -o $@ -Wl,-soname=$(ROTE_SONAME) $(OBJECTS) $(LDFLAGS) $(LIBS)

clean:
	rm -f *.o $(ROTE_LIB_NAME).*

pristine: clean
	rm -rf autom4te.cache configure config.status config.log Makefile rote-config rote.h

.PHONY: clean all install pristine

