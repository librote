/*
LICENSE INFORMATION:
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License (LGPL) as published by the Free Software Foundation.

Please refer to the COPYING file for more information.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

Copyright (c) 2007 Ryo Dairiki
*/

#if !defined(UTF8_H_) && defined(USE_UTF8)
#define UTF8_H_

#include "rote.h"

/* Erase a wide character and fill empty strings there */
void rote_utf8_erase_character(RoteTerm *rt, size_t row, size_t col);

#endif
