/*
LICENSE INFORMATION:
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License (LGPL) as published by the Free Software Foundation.

Please refer to the COPYING file for more information.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

Copyright (c) 2004 Bruno T. C. de Oliveira
*/


#include "rote.h"
#include "utf8.h"
#include "roteprivate.h"
#include "inject_csi.h"
#include <string.h>
#include <stdbool.h>
#include <stdio.h>

static void cursor_line_down(RoteTerm *rt) {
   int i;
   rt->crow++;
   rt->curpos_dirty = true;
   if (rt->crow <= rt->pd->scrollbottom) return;

   /* must scroll the scrolling region up by 1 line, and put cursor on 
    * last line of it */
   rt->crow = rt->pd->scrollbottom;
   
   for (i = rt->pd->scrolltop; i < rt->pd->scrollbottom; i++) {
      rt->line_dirty[i] = true;
      memcpy(rt->cells[i], rt->cells[i+1], sizeof(RoteCell) * rt->cols);
   }
      
   rt->line_dirty[rt->pd->scrollbottom] = true;

   /* clear last row of the scrolling region */
   for (i = 0; i < rt->cols; i++) {
      clear_cell(&rt->cells[rt->pd->scrollbottom][i]);
   }

}

static void cursor_line_up(RoteTerm *rt) {
   int i;
   rt->crow--;
   rt->curpos_dirty = true;
   if (rt->crow >= rt->pd->scrolltop) return;

   /* must scroll the scrolling region up by 1 line, and put cursor on 
    * first line of it */
   rt->crow = rt->pd->scrolltop;
   
   for (i = rt->pd->scrollbottom; i > rt->pd->scrolltop; i--) {
      rt->line_dirty[i] = true;
      memcpy(rt->cells[i], rt->cells[i-1], sizeof(RoteCell) * rt->cols);
   }
      
   rt->line_dirty[rt->pd->scrolltop] = true;

   /* clear first row of the scrolling region */
   for (i = 0; i < rt->cols; i++) {
      clear_cell(&rt->cells[rt->pd->scrolltop][i]);
   }

}

static inline void put_normal_char(RoteTerm *rt, char_t c) {
#ifdef USE_UTF8
   const ssize_t width = wcwidth (c);
   if (width < 0) return;
   if (rt->ccol + width > rt->cols) {
#else
   if (rt->ccol >= rt->cols) {
#endif
      rt->ccol = 0;
      cursor_line_down(rt);
   }

   if (rt->insert) {
       int i;

       for(i = rt->cols - 1; i >= rt->ccol+1; i--)
	   rt->cells[rt->crow][i] = rt->cells[rt->crow][i-1];
   }

#ifdef USE_UTF8
   rote_utf8_erase_character(rt, rt->crow, rt->ccol);
   if (width > 1) rote_utf8_erase_character(rt, rt->crow + width - 1, rt->ccol);

   int i;
   for (i = 0; i < width; ++i) {
      rt->cells[rt->crow][rt->ccol + i].fcount = width;
      rt->cells[rt->crow][rt->ccol + i].empty = false;
      rt->cells[rt->crow][rt->ccol + i].findex = i;
      if (i == 0) {
         rt->cells[rt->crow][rt->ccol + i].ch = c;
      } else {
         rt->cells[rt->crow][rt->ccol + i].ch = 0x20;
      }
      rt->cells[rt->crow][rt->ccol + i].attr = rt->curattr;
   }
   rt->ccol += width;
#else
# ifdef USE_NCURSES
   /* translate from Code Page 437 to ACS_* values */
   char_t ch = (unsigned char)c;
   if(ch >= 128){
      switch(ch){
         case 218: c = ACS_ULCORNER; break;
         case 192: c = ACS_LLCORNER; break;
         case 191: c = ACS_URCORNER; break;
         case 217: c = ACS_LRCORNER; break;
         case 195: c = ACS_LTEE; break;
         case 180: c = ACS_RTEE; break;
         case 193: c = ACS_BTEE; break;
         case 194: c = ACS_TTEE; break;
         case 196: c = ACS_HLINE; break;
         case 179: c = ACS_VLINE; break;
         case 197: c = ACS_PLUS; break;
         /* --- */
         case 177: c = ACS_CKBOARD; break;
         case 248: c = ACS_DEGREE; break;
         case 241: c = ACS_PLMINUS; break;
         case 254: c = ACS_BULLET; break;
         /* --- */
         case 176: c = ACS_BOARD; break;
         case 206: c = ACS_LANTERN; break;
         case 219: c = ACS_BLOCK; break;
         case 243: c = ACS_LEQUAL; break;
         case 242: c = ACS_GEQUAL; break;
         case 227: c = ACS_PI; break;
         case 216: c = ACS_NEQUAL; break;
         case 156: c = ACS_STERLING; break;
      }
   }
# endif
   rt->cells[rt->crow][rt->ccol].ch = c;
   rt->cells[rt->crow][rt->ccol].attr = rt->curattr;
   rt->ccol++;
#endif
   rt->line_dirty[rt->crow] = true;
   rt->curpos_dirty = true;
}

static inline void put_graphmode_char(RoteTerm *rt, char_t c) {
   char_t nc;
   /* do some very pitiful translation to regular ascii chars */
   switch (c) {
      case 'j': case 'k': case 'l': case 'm': case 'n': case 't': 
                                    case 'u': case 'v': case 'w':
         nc = '+'; break;
      case 'x':
         nc = '|'; break;
      default:
         nc = '%';
   }

   put_normal_char(rt, nc);
}

static inline void new_escape_sequence(RoteTerm *rt) {
   rt->pd->escaped = true;
   rt->pd->esbuf_len = 0;
   rt->pd->esbuf[0] = '\0';
}

static inline void cancel_escape_sequence(RoteTerm *rt) {
   rt->pd->escaped = false;
   rt->pd->esbuf_len = 0;
   rt->pd->esbuf[0] = '\0';
}

static void handle_control_char(RoteTerm *rt, char c) {
   switch (c) {
      case '\r': rt->ccol = 0; break; /* carriage return */
      case '\n':  /* line feed */
         rt->ccol = 0; cursor_line_down(rt);
         rt->curpos_dirty = true;
         break;
      case '\b': /* backspace */
         if (rt->ccol > 0) rt->ccol--;
         rt->curpos_dirty = true;
         break;
      case '\t': /* tab */
         rt->ccol += 8 - (rt->ccol % 8);
         clamp_cursor_to_bounds(rt);
         break;
      case '\x1B': /* begin escape sequence (aborting previous one if any) */
         new_escape_sequence(rt);
         break;
      case '\x0E': /* enter graphical character mode */
         rt->pd->graphmode = true;
         break;
      case '\x0F': /* exit graphical character mode */
         rt->pd->graphmode = false;
         break;
      case '\x9B': /* CSI character. Equivalent to ESC [ */
         new_escape_sequence(rt);
         rt->pd->esbuf[rt->pd->esbuf_len++] = '[';
         break;
      case '\x18': case '\x1A': /* these interrupt escape sequences */
         cancel_escape_sequence(rt);
         break;
      case '\a': /* bell */
         /* do nothing for now... maybe a visual bell would be nice? */
         break;
      #ifdef DEBUG
      default:
         fprintf(stderr, "Unrecognized control char: %d (^%c)\n", c, c + '@');
         break;
      #endif
   }
}

static inline bool is_valid_csi_ender(char c) {
   return (c >= 'a' && c <= 'z') ||
          (c >= 'A' && c <= 'Z') ||
          c == '@' || c == '`';
}

static void try_interpret_escape_seq(RoteTerm *rt) {
   char firstchar = rt->pd->esbuf[0];
   char lastchar  = rt->pd->esbuf[rt->pd->esbuf_len-1];

   if (!firstchar) return;  /* too early to do anything */

   if (rt->pd->handler) {
      /* call custom handler */
      #ifdef DEBUG
      fprintf(stderr, "Calling custom handler for ES <%s>.\n", rt->pd->esbuf);
      #endif

      int answer = (*(rt->pd->handler))(rt, rt->pd->esbuf);
      if (answer == ROTE_HANDLERESULT_OK) {
         /* successfully handled */
         #ifdef DEBUG
         fprintf(stderr, "Handler returned OK. Done with escape sequence.\n");
         #endif

         cancel_escape_sequence(rt);
         return;
      }
      else if (answer == ROTE_HANDLERESULT_NOTYET) {
         /* handler might handle it when more characters are appended to 
          * it. So for now we don't interpret it */
         #ifdef DEBUG
         fprintf(stderr, "Handler returned NOTYET. Waiting for more chars.\n");
         #endif

         return;
      }
   
      /* If we got here then answer == ROTE_HANDLERESULT_NOWAY */
      /* handler said it can't handle that escape sequence,
       * but we can still try handling it ourselves, so 
       * we proceed normally. */
      #ifdef DEBUG
      fprintf(stderr, "Handler returned NOWAY. Trying our handlers.\n");
      #endif
   }

   /* interpret ESC-M as reverse line-feed */
   if (firstchar == 'M') {
      cursor_line_up(rt);
      cancel_escape_sequence(rt);
      return;
   }

   if (firstchar != '[' && firstchar != ']') {
      /* unrecognized escape sequence. Let's forget about it. */
      #ifdef DEBUG
      fprintf(stderr, "Unrecognized ES: <%s>\n", rt->pd->esbuf);
      #endif

      cancel_escape_sequence(rt);
      return;
   }

   if (firstchar == '[' && is_valid_csi_ender(lastchar)) {
      /* we have a csi escape sequence: interpret it */
      rote_es_interpret_csi(rt);
      cancel_escape_sequence(rt);
   }
   else if (firstchar == ']' && lastchar == '\a') {
      /* we have an xterm escape sequence: interpret it */

      /* rote_es_interpret_xterm_es(rt);     -- TODO!*/
      #ifdef DEBUG
      fprintf(stderr, "Ignored XTerm ES.\n");
      #endif
      cancel_escape_sequence(rt);
   }

   /* if the escape sequence took up all available space and could
    * not yet be parsed, abort it */
   if (rt->pd->esbuf_len + 1 >= ESEQ_BUF_SIZE) cancel_escape_sequence(rt);
}
   
void rote_vt_inject(RoteTerm *rt, const char *data, int len) {
   int i;
   for (i = 0; i < len; i++, data++) {
#ifdef USE_UTF8
      wchar_t wc;
      unsigned char mbc[6];
      size_t mbc_length = 0;

      if (rt->pd->utf8_index > 0) {
         rt->pd->utf8_buffer[rt->pd->utf8_index] = ((unsigned char) *data) & 0x3f;
         ++rt->pd->utf8_index;
         if (rt->pd->utf8_index >= 6) {
            memcpy (mbc, rt->pd->utf8_buffer, sizeof (char) * 6);
            mbc_length = rt->pd->utf8_length;
            wc = /*(rt->pd->utf8_buffer[0] << 30) | (rt->pd->utf8_buffer[1] << 24) | */
                  (rt->pd->utf8_buffer[2] << 18) | (rt->pd->utf8_buffer[3] << 12) | 
                  (rt->pd->utf8_buffer[4] << 6) | (rt->pd->utf8_buffer[5] << 0);
            rt->pd->utf8_index = 0;
            rt->pd->utf8_buffer[0] = 0;
            rt->pd->utf8_buffer[1] = 0;
            rt->pd->utf8_buffer[2] = 0;
            rt->pd->utf8_buffer[3] = 0;
            rt->pd->utf8_buffer[4] = 0;
            rt->pd->utf8_buffer[5] = 0;
         } else {
            continue;
         }
      } else {
         const unsigned char c = *data;
         if (c < 0x80) {
            rt->pd->utf8_index = 0;
            rt->pd->utf8_length = 1;
            mbc[0] = c;
            mbc_length = 1;
            wc = c;
         } else if (0xc0 <= c && c < 0xe0) {
            rt->pd->utf8_buffer[4] = c & 0x1f;
            rt->pd->utf8_index = 5;
            rt->pd->utf8_length = 2;
            continue;
         } else if (0xe0 <= c && c < 0xf0) {
            rt->pd->utf8_buffer[3] = c & 0x0f;
            rt->pd->utf8_index = 4;
            rt->pd->utf8_length = 3;
            continue;
         } else if (0xf0 <= c && c < 0xf8) {
            rt->pd->utf8_buffer[2] = c & 0x07;
            rt->pd->utf8_index = 3;
            rt->pd->utf8_length = 4;
            continue;
         } else if (0xf8 <= c && c < 0xfc) {
            rt->pd->utf8_buffer[1] = c & 0x03;
            rt->pd->utf8_index = 2;
            rt->pd->utf8_length = 5;
            continue;
         } else if (0xfc <= c && c < 0xfe) {
            rt->pd->utf8_buffer[0] = c & 0x01;
            rt->pd->utf8_index = 1;
            rt->pd->utf8_length = 6;
            continue;
         } else {
            // Invalid utf8 sequence, how should I do?
            rt->pd->utf8_index = 0;
            rt->pd->utf8_length = 1;
            mbc[0] = c;
            mbc_length = 1;
            wc = c;
         }
      }

      if (wc == 0) continue;  /* completely ignore NUL */

      if (wc >= 1 && wc <= 31) {
         handle_control_char(rt, wc);
         continue;
      }

      if (rt->pd->escaped) {
         if (rt->pd->esbuf_len + mbc_length < ESEQ_BUF_SIZE) {
            memcpy(&rt->pd->esbuf[rt->pd->esbuf_len], mbc, sizeof (char) * mbc_length);
            rt->pd->esbuf[rt->pd->esbuf_len + mbc_length] = 0;
            rt->pd->esbuf_len += mbc_length;
            try_interpret_escape_seq(rt);
         } else {
            cancel_escape_sequence(rt);
            break;
         }
      } else if (rt->pd->graphmode)
         put_graphmode_char(rt, wc);
      else
         put_normal_char(rt, wc);
#else
      if (*data == 0) continue;  /* completely ignore NUL */
      if (*data >= 1 && *data <= 31) {
         handle_control_char(rt, *data);
         continue;
      }

      if (rt->pd->escaped && rt->pd->esbuf_len < ESEQ_BUF_SIZE) {
         /* append character to ongoing escape sequence */
         rt->pd->esbuf[rt->pd->esbuf_len] = *data;
         rt->pd->esbuf[++rt->pd->esbuf_len] = 0;

         try_interpret_escape_seq(rt);
      }
      else if (rt->pd->graphmode)
         put_graphmode_char(rt, *data);
      else
         put_normal_char(rt, *data);
#endif
   }
}

