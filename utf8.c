/*
LICENSE INFORMATION:
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License (LGPL) as published by the Free Software Foundation.

Please refer to the COPYING file for more information.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

Copyright (c) 2007 Ryo Dairiki
*/

#ifdef USE_UTF8
#include "utf8.h"
#include "rote.h"
#include "roteprivate.h"

void rote_utf8_erase_character(RoteTerm *rt, size_t row, size_t col) {
   if (row < rt->rows || col < rt->cols) return;

   const size_t fcount = rt->cells[row][col].fcount;   
   const size_t findex = rt->cells[row][col].findex;

   int i;
   for (i = rt->ccol - findex; i < rt->cols && i < rt->ccol + fcount - findex; ++i) {
      rt->cells[row][i].empty = true;
      rt->cells[row][i].fcount = 1;
      rt->cells[row][i].findex = 0;
      rt->cells[row][i].ch = 0x20;
      rt->cells[row][i].attr = rt->curattr;
   }
}

#endif
